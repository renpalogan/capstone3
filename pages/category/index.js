import React, { useState, useEffect } from 'react';
import {Form, Button, Row, Col, Container} from 'react-bootstrap';
import Swal from 'sweetalert2'
import UserContext from '../../UserContext';
import Router from 'next/router';
import Head from 'next/head';
// import View from '../../components/View'
import AppHelper from '../../app-helper'

export default function Home() {

    const [categoryName, setCategoryName] = useState("")
    const [categoryType, setCategoryType] = useState("")
    const [isActive, setIsActive] = useState(false)
        
    useEffect(() => {
        // validation to enable submit button when all fields are populated and passwords match

        if ( categoryName!== "" && categoryType!== "" ) {

            setIsActive(true)

        } else {
            
            setIsActive(false)
        }
    }, [categoryName, categoryType])


    function addCategory(e) {
        e.preventDefault()

        const myToken = localStorage.getItem('token')
        const options = {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${myToken}`, 
                'Content-Type': 'application/json'
             },
            body: JSON.stringify({

                category_name: categoryName,
                category_type: categoryType
            })
        }


                fetch(`${ AppHelper.API_URL }/users/category`, options)
                .then(res => res.json())
                .then(data => {
                    console.log(data)

                    if(data === true){
                        Swal.fire(`Transaction successful. `)
                        console.log(`Registration successful.`)
                     }else {
                        Swal.fire('Transaction Failed', 'error')
                        console.log('Transaction failed')
                    }
                }) 
                

                setCategoryName("")
                setCategoryType("")

        console.log(`${categoryName} with Type ${categoryType} and has been added`)
    }




        return (
            <React.Fragment>
                <Head>
                    <title>Home Page</title>
                </Head>
                <Container className="mt-4">
                    <Row className="justify-content-center">
                        <Col xs={6}>
                            <Form onSubmit={e => addCategory(e)}>
                            <Form.Label><h2>Add New Category</h2></Form.Label>
                                <Form.Group>
                                        {/* <Form.Label></Form.Label> */}
                                    <Form.Control type="categoryName" placeholder="Category Name" value={categoryName}
                                    onChange={e => setCategoryName(e.target.value)} required>
                                        
                                    </Form.Control>
                                    <Form.Label></Form.Label>
                                    <Form.Control type="categoryType" as="select" placeholder="Choose Category" value={categoryType}
                                    onChange={e => setCategoryType(e.target.value)} required >
                                        <option>Category Type</option>
                                        <option>Income</option>
                                        <option>Expense</option>
                                    </Form.Control>
                                    </Form.Group>
                                    {isActive ?
                                        <Button variant="info" type="submit" id="submitBtn" size="lg" block>
                                            Submit
                                        </Button> 
                                        :
                                        <Button variant="info" type="submit" id="submitBtn" size="lg" block disabled>
                                            Submit
                                        </Button> 
                                    }
                            </Form>
                        </Col >
                    </Row>
                </Container>
            </React.Fragment>


        )
    }