import { useContext, useEffect } from 'react';
import Router from 'next/router'
// import { Redirect } from 'react-router-dom';
import UserContext from '../../UserContext';


export default function index() {
    const { unsetUser, setUser } = useContext(UserContext);   

    useEffect(() => {
        unsetUser();
        Router.push('/login')
    }, [])

    return null;
}