import React, { useState, useEffect } from 'react';
import {Form, Button, Row, Col, Container} from 'react-bootstrap';
import Swal from 'sweetalert2'
import UserContext from '../../UserContext';
import Router from 'next/router';
import Head from 'next/head';
import View from '../../components/View'
import AppHelper from '../../app-helper'

export default function index() {
    return(
		<View title={ `Register` }>
			<Row className="justify-content-center">
				<Col xs md="10">
					<h3>Register</h3>
					<RegisterForm />
				</Col>
			</Row>
		</View>

    )
}


    const RegisterForm = () => {   

        const [email, setEmail] = useState("")
        const [password1, setPassword1] = useState("")
        const [password2, setPassword2] = useState("")
        const [isActive, setIsActive] = useState(false)

        
        
        useEffect(() => {
            // validation to enable submit button when all fields are populated and passwords match

            if( (email !== "" && password1 !=="" && password2 !=="") && ( password1 === password2) ) {

                setIsActive(true)

            } else {
                
                setIsActive(false)
            }
        }, [email, password1, password2])

        function registeredUser(e) {
            e.preventDefault()

            const options = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    email: email,
                    password: password1
                })
            }

            fetch(`${ AppHelper.API_URL }/users/email-exists`, options)
            .then(res => res.json())
            .then(data => {

                console.log(data)
                //if true(duplicate email)return alert
                //else fetching
                if(data === false){
                    //if no duplicates found
                    //get the routes for registration in our server
                    fetch(`${ AppHelper.API_URL }/users/`, options)
                    .then(res => res.json())
                    .then(data => {
                        console.log(data)
                        Router.push('/login')
                        //if registration is successful
                        if(data === true){
                            Swal.fire(`Registration successful. User ${email} registered`)
                            console.log(`Registration successful. User ${email} registered with password ${password1}`)
                         }
                    })
                } else {
                    Swal.fire('Duplicate Email', 'error')
                    console.log('Email Already Exist')
                }
            })




            setEmail("")
            setPassword1("")
            setPassword2("")

            
            // console.log(`Registration successful. User ${email} registered with password ${password1}`)
        }

        return (
            <React.Fragment>
                <Head>
                    <title>Register Authentication</title>
                </Head>
                <Container className="mt-5">
                    <Row>
                        <Col>
                            <Form onSubmit={e => registeredUser(e)}>
                                <Form.Group controlId="userEmail">
                                    <Form.Label>Email:</Form.Label>
                                    <Form.Control type="email" placeholder="Enter your email" value={email} 
                                    onChange={e => setEmail(e.target.value)} required 
                                    />
                                    <Form.Text className="text-muted">
                                        We will never share your email with anyone else
                                    </Form.Text>
                                </Form.Group>
                                <Form.Group controlId="password1">
                                    <Form.Label>Input Password1</Form.Label>
                                    <Form.Control type="password" placeholder="Enter Password" value={password1} 
                                    onChange={e => setPassword1(e.target.value)} required/>
                                </Form.Group>
                                <Form.Group controlId="password2">
                                    <Form.Label>Verify Password</Form.Label>
                                    <Form.Control type="password" placeholder="password2" value={password2} 
                                    onChange={e => setPassword2(e.target.value)} required/>
                                </Form.Group>
                                {isActive ?
                                    <Button variant="info" type="submit" id="submitBtn" size="lg" block>
                                        Submit
                                    </Button> 
                                    :
                                    <Button variant="info" type="submit" id="submitBtn" size="lg" block disabled>
                                        Submit
                                    </Button> 
                                }
                            </Form>
                        </Col >
                    </Row>
                </Container>
            </React.Fragment>
        )
    }



            