import React, { useEffect, useState } from 'react'
import NavBar from '../components/NavBar'
import { Container } from 'react-bootstrap'
import { UserProvider } from '../UserContext'
import AppHelper from '../app-helper';
import { Router } from 'next/router';
// import user from '../data/user';

import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css';

export default function MyApp({ Component, pageProps }) {

        //global user state
        const [user, setUser] = useState({
          //user state is an object with properties from our local storage
          id: null,
          isAdmin: null
        })
  
        //function to clear local storage upon logout
        const unsetUser = () => {
          localStorage.clear();
  
          setUser({
            id: null,
            isAdmin: null
          })
  }

  useEffect(() => {
      
    const options = {
      headers: { Authorization: `Bearer ${ AppHelper.getAccessToken() }`}
    }

    fetch(`${ AppHelper.API_URL }/users/details`, options)
    .then(AppHelper.toJSON)
    .then(data => {
      if( typeof data.id !== undefined ) {
        setUser({ id: data._id, isAdmin: data.isAdmin })
        
      } else {
        setUser({ id: null, isAdmin: null })
      }
    })
  }, [user.id])

  useEffect(() => {
		console.log(`User with id: ${user.id} is an admin: ${user.isAdmin}`)
	}, [user.isAdmin, user.id])


  return (
      <React.Fragment>
        <UserProvider value={{user, setUser, unsetUser}}>
          <NavBar/>
          <Container>
              <Component {...pageProps} />
          </Container>
        </UserProvider>
      </React.Fragment>
  )

}
