import React from 'react'
import {Row, Col, Form, Container, Table, Card, Button} from 'react-bootstrap';
import { useEffect, useState } from 'react'
import AppHelper from '../../app-helper'
// import { useBootstrapPrefix } from 'react-bootstrap/esm/ThemeProvider';

export default function index() {

    const [transactionData, setTransactionData] = useState([])
    const [filterCategory, setFilterCategory] = useState("All")
    const [search, setSearch] = useState("")

    useEffect(() =>{


            const myToken = localStorage.getItem('token')
            const options = {

                headers: {
                    Authorization: `Bearer ${myToken}`, 
                }
            }

            fetch(`${ AppHelper.API_URL }/users/getTransaction`, options)
            .then(res => res.json())
            .then(data => { 
                
                const records = data.filter((type) => {

                    if(filterCategory === 'All') {
                        return true
                    } else if (filterCategory === type.transaction_type) {
                        return true
                    } else if(search < data.length) {
                        return true
                    } else if (search === type.description) {
                        return true
                    } else if (search === type.category) {
                        return true
                    }
                })
                // console.log('hello')
                
                const transactionRows = records.map(transaction => {
                // console.log(transaction)
                // let td = transaction.transaction_date
                // let tdate = td.substr(0,10)

                let tdate = new Date(transaction.transaction_date)
                let date = (tdate.getMonth()+1) + '/'
                            + tdate.getDate() + '/'
                            + tdate.getFullYear()

                return (
                    <tr key={transaction._id}>
                        <td>{date}</td>
                        <td>{transaction.description}</td>
                        <td>{transaction.amount}</td>
                        <td>{transaction.transaction_type}</td>
                        <td>{transaction.category}</td>
                    </tr>
                )
            })
                
        setTransactionData(transactionRows)  
        })



    },[filterCategory])

    // console.log(filterCategory)


    useEffect(() =>{


        const myToken = localStorage.getItem('token')
        const options = {

            headers: {
                Authorization: `Bearer ${myToken}`, 
            }
        }

        fetch(`${ AppHelper.API_URL }/users/getTransaction`, options)
        .then(res => res.json())
        .then(data => { 
            
            const records = data.filter((type) => {
                // console.log(type)

                if(search < data.length) {
                    return true
                } else if (search === type.description) {
                    return true
                } else if (search === type.category) {
                    return true
                } 
            })
            // console.log(records)
            
            const transactionRows = records.map(transaction => {
            // console.log(transaction)
            // let td = transaction.transaction_date
            // let tdate = td.substr(0,10)

            let tdate = new Date(transaction.transaction_date)
            let date = (tdate.getMonth()+1) + '/'
                        + tdate.getDate() + '/'
                        + tdate.getFullYear()

            return (
                <tr key={transaction._id}>
                    <td>{date}</td>
                    <td>{transaction.description}</td>
                    <td>{transaction.amount}</td>
                    <td>{transaction.transaction_type}</td>
                    <td>{transaction.category}</td>
                </tr>
            )
        })
            
    setTransactionData(transactionRows)  
    })



},[search])


    return (
        <React.Fragment>
        <Container>
            <Form className="mt-4">
                <Form.Row>
                    <Col xs={6}>
                        <Form.Group controlId="search">
                        <Form.Control type="search" placeholder="Search" value={search} 
                        onChange={e => setSearch(e.target.value)} required />
                        </Form.Group>
                    </Col>
                    <Col xs={6}>
                        <Form.Control placeholder="None" as="select" value={filterCategory} 
                                    onChange={e => setFilterCategory(e.target.value)} required>
                            <option>All</option>
                            <option>Income</option>
                            <option>Expense</option>
                        </Form.Control>
                    </Col>
                    {/* <Col xs={4}>
                        <Form.Control placeholder="None" as="select">
                            <option>None</option>
                        </Form.Control>
                    </Col> */}
                </Form.Row>
            </Form>
        </Container>
        <Container className="mt-4">
            <Row>
                <Col xs={12}>
                <Card className="">
                    <Card.Body>
                        <Card.Title></Card.Title>
                        <Table>
                            <thead>
                                <td><em><strong>Date</strong></em></td>
                                <td><em><strong>Description</strong></em></td>
                                <td><em><strong>Amount</strong></em></td>
                                <td><em><strong>Transaction type</strong></em></td>
                                <td><em><strong>Category</strong></em></td>
                            </thead>
                            <tbody>{transactionData}</tbody>
                        </Table>
                    </Card.Body>
                </Card>
                </Col>
            </Row>
        </Container>
        </React.Fragment>
        
    )
}






