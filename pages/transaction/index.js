import React, { useState, useEffect } from 'react';
import {Form, Button, Row, Col, Container, Card, Table} from 'react-bootstrap';
import Swal from 'sweetalert2'
import Head from 'next/head';
// import UserContext from '../../UserContext';
// import Router from 'next/router';
// import View from '../../components/View'
import AppHelper from '../../app-helper'
import DoughnutChart from '../../components/DoughnutChart'

export default function Home() {

    const [description, setDescription] = useState("")
    const [amount, setAmount] = useState("")
    const [type, setType] = useState("")
    const [category, setCategory] = useState("")
    const [balance, setBalance] = useState("")
    const [income, setIncome] = useState("")
    const [expense, setExpense] = useState("")
    const [isActive, setIsActive] = useState(false)
    const [categoryList, setCategoryList] = useState([])
    const [transactionData, setTransactionData] = useState([])
    const [filterCategory, setFilterCategory] = useState("All")

    useEffect(() => {


        const myToken = localStorage.getItem('token')
        const options = {

            headers: {
                Authorization: `Bearer ${myToken}`, 
            }
        }

        fetch(`${ AppHelper.API_URL }/users/getTransaction`, options)
        .then(res => res.json())
        .then(data => { 
            
            const records = data.filter((element) => {

                if(filterCategory === 'All') {
                    return true
                } else if (filterCategory === element.transaction_type) {
                    return true
                }
            })
            // console.log('hello')
            
            const transactionRows = records.map(transaction => {
            // console.log(transaction)
            // let td = transaction.transaction_date
            // let tdate = td.substr(0,10)

            let tdate = new Date(transaction.transaction_date)
            let date = (tdate.getMonth()+1) + '/'
                        + tdate.getDate() + '/'
                        + tdate.getFullYear()

            return (
                <tr key={transaction._id}>
                        <td>{date}</td>
                        <td>{transaction.description}</td>
                        <td>{transaction.amount}</td>
                        <td>{transaction.transaction_type}</td>
                        <td>{transaction.category}</td>
                </tr>
            )
        })
            
    setTransactionData(transactionRows)  
    })



},[filterCategory])

        
    useEffect(() => {
        // validation to enable submit button when all fields are populated and passwords match

        if ( description !== "" && amount !=="" && type !=="" && category!=="" ) {

            setIsActive(true)

        } else {
            
            setIsActive(false)
        }
    }, [description, amount, type, category])


    useEffect(() =>{

        const myToken = localStorage.getItem('token')
        const options = {

            headers: {
                Authorization: `Bearer ${myToken}`, 
             }
        }

        fetch(`${ AppHelper.API_URL }/users/getCategory`, options)
        .then(res => res.json())
        .then(data => { 

            const categoryArray = data.filter((category) => {

                if (type === category.category_type) {
                    return true
                }
            })

            console.log(categoryArray)

            const categoryOptions = categoryArray.map(category => {
                console.log(category)
                return (
                    <option key={category._id}>{category.category_name}</option>
                )
            })
            setCategoryList(categoryOptions)

            
        })


    },[type])



    function addTransaction(e) {
        e.preventDefault()
        console.log(category)

        const myToken = localStorage.getItem('token')
        const options = {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${myToken}`, 
                'Content-Type': 'application/json'
             },
            body: JSON.stringify({
                description: description,
                amount: amount,
                transaction_type: type,
                category: category
            })
        }


                fetch(`${ AppHelper.API_URL }/users/transaction`, options)
                .then(AppHelper.toJSON)
                .then(data => {
                    console.log(data)

                    if(data === true){
                        Swal.fire(`Transaction successful. `)
                        console.log(`Registration successful.`)
                     }else {
                        Swal.fire('Transaction Failed', 'error')
                        console.log('Transaction failed')
                    }
                }) 



        setDescription("")
        setAmount("")
        setType("")
        setCategory("")
        // setCategoryName("")
        // setCategoryType("")

        console.log(`${description} with ${amount} and has type ${type} and `)
    }


        return (
            <React.Fragment>
                <Head>
                    <title>Add Transaction Page</title>
                </Head>
                <Container className="mt-4">
                    <Row>
                        <Col xs={4}>
                            <Form onSubmit={e => addTransaction(e)}>
                            <Form.Label><h2>Add New Transaction</h2></Form.Label>
                                <div><h3>Balance: </h3></div>
                                {/* <br></br> */}
                                {/* <div><h4>Income: </h4></div>
                                <div><h4>Expenses: </h4></div> */}
                                <Form.Group controlId="description"> 
                                    <Form.Label></Form.Label>
                                    <Form.Control type="description" placeholder="Description" value={description}
                                    onChange={e => setDescription(e.target.value)} required />
                                </Form.Group>
                                <Form.Group controlId="amount">
                                            {/* <Form.Label></Form.Label> */}
                                        <Form.Control type="amount" placeholder="Amount" value={amount}
                                        onChange={e => setAmount(e.target.value)} required />
                                </Form.Group>
                                <Form.Group controlId="type">
                                        {/* <Form.Label></Form.Label> */}
                                    <Form.Control type="type" as="select" placeholder="Transaction Type" value={type}
                                    onChange={e => setType(e.target.value)} required >
                                        <option>Transaction Type</option>
                                        <option>Income</option>
                                        <option>Expense</option>
                                    </Form.Control>
                                </Form.Group>
                                <Form.Group>
                                        {/* <Form.Label></Form.Label> */}
                                    <Form.Control type="category" as="select" placeholder="Choose Category" value={category}
                                    onChange={e => setCategory(e.target.value)} required >
                                        <option>Category Type</option>
                                                {categoryList}
                                    </Form.Control>
                                </Form.Group>
                                    {isActive 
                                        ?
                                        <Button variant="info" type="submit" id="submitBtn" size="lg" block>
                                            Submit
                                        </Button> 
                                        :
                                        <Button variant="info" type="submit" id="submitBtn" size="lg" block disabled>
                                            Submit
                                        </Button> 
                                    }
                            </Form>
                        </Col >
                        <Col xs={8}>
                        <h2> Transaction</h2>
                            <Card className="">
                                <Card.Body>
                                    <Card.Title></Card.Title>
                                    <Table>
                                        <thead>       
                                            <td><em><strong>Date</strong></em></td>
                                            <td><em><strong>Description</strong></em></td>
                                            <td><em><strong>Amount</strong></em></td>
                                            <td><em><strong>Type</strong></em></td>
                                            <td><em><strong>Category</strong></em></td>
                                        </thead>
                                        <tbody>{transactionData}</tbody>
                                    </Table>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                </Container>

            </React.Fragment>


        )
    }



