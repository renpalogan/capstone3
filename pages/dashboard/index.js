import React, { useState, useEffect } from 'react';
import {Row, Col, Card, Container} from 'react-bootstrap';
import AppHelper from '../../app-helper'
import DoughnutChart from '../../components/DoughnutChart'
import BarChart from '../../components/BarChart'
import LineChart from '../../components/LineChart'
import PieChart from '../../components/PieChart'

export default function dashBoard() {

    const [income, setIncome] = useState("")
    const [expense, setExpense] = useState("")

    useEffect(() => {

        // console.log(income)

        const myToken = localStorage.getItem('token')
        const options = {

            headers: {
                Authorization: `Bearer ${myToken}`
            }
        }

        fetch(`${ AppHelper.API_URL }/users/getTransaction`, options)
        .then(res => res.json())
        .then(data => { 

            if(data !== undefined ) {
                let tempExpense = 0
                let tempIncome = 0
                data.forEach(transaction => {

                    const type = transaction.transaction_type
                    const amount = transaction.amount

                    // console.log(type)
                    // console.log(transaction.transaction_type)
    
                    if( type === 'Income' ) {
                        tempIncome += amount
                    } else if (type === 'Expense') {
                        tempExpense += amount
                    } console.log(tempIncome)

                }) 

                setIncome(tempIncome)
                setExpense(tempExpense)
            } 
        })
            
 
    },[])
    console.log(typeof(income))
    



    return (
        <Container>
        <Row className="mt-5">
            <Col md={6}>
                <Card >
                    <Card.Body>
                    <Card.Title>
                    Bar Graph
                    </Card.Title>
                    <Card.Text>
                    </Card.Text>
                    <BarChart Income={income} Expense={expense}/>
                    </Card.Body>
                </Card>
            </Col>
            <Col md={6}>
                <Card >
                    <Card.Body>
                    <Card.Title>
                    Doughnut Chart
                    </Card.Title>
                    <Card.Text>
                    </Card.Text>
                    <DoughnutChart Income={income} Expense={expense}/>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
        <Row className="mt-4">
            <Col md={6}>
                <Card >
                    <Card.Body>
                    <Card.Title>
                    Line Graph
                    </Card.Title>
                    <Card.Text>
                    </Card.Text>
                    <LineChart Income={income} Expense={expense}/>
                    </Card.Body>
                </Card>
            </Col>
            <Col md={6}>
                <Card >
                    <Card.Body>
                    <Card.Title>
                    Pie Chart
                    </Card.Title>
                    <Card.Text>
                    </Card.Text>
                    <PieChart Income={income} Expense={expense}/>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
        </Container>
    )
}






