import React, { useContext } from 'react';
import {Navbar, Nav, NavDropdown} from 'react-bootstrap';
import UserContext from '../UserContext'
import Link from 'next/link'

export default function NavBar() {

  const { user } = useContext(UserContext)
  

    return (
      
      <Navbar expand="lg" bg="dark" variant="dark" sticky="top">
      <Link href="/">
          <a className="navbar-brand">Budget Tracker</a>
      </Link>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="justify-content-right">
              {/* <Link href="/courses">
                  <a className="nav-link" role="button">Courses</a>
              </Link> */}
              
              {(user.id !== undefined)
                  ? 
                      <React.Fragment>
                          <Link href="/transaction">
                              <a className="nav-link" role="button">Home</a>
                          </Link>
                          <Link href="/category">
                              <a className="nav-link" role="button">Add Category</a>
                          </Link>
                          <Link href="/history">
                              <a className="nav-link" role="button">History</a>
                          </Link>
                          <Link href="/dashboard">
                              <a className="nav-link" role="button">Dashboard</a>
                          </Link>
                          <Link href="/logout">
                              <a className="nav-link" role="button">Logout</a>
                          </Link>
                      </React.Fragment>
                  : 
                  <React.Fragment>
                      <Link href="/register">
                          <a className="nav-link" role="button">Register</a>
                      </Link>
                      <Link href="/login">
                          <a className="nav-link" role="button">SignIn</a>
                      </Link>
                  </React.Fragment>
              }
          </Nav>
      </Navbar.Collapse>
  </Navbar>
)
}
