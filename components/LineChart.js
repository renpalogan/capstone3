import React, {useState} from 'react';
import { Doughnut, Line } from 'react-chartjs-2';


export default function LineChart({Income, Expense}) {

 console.log(Income, Expense)

    return (

        <React.Fragment>
            <Line
            data={{
            datasets:[{
                data: [Income, Expense],
                backgroundColor: ["Blue", "Brown"]
            }],
            labels: ["Income", "Expense"]
            }}
            redraw={false}
        />
        </React.Fragment>
    )

}