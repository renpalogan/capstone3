import React, {useState} from 'react';
import { Doughnut, Pie } from 'react-chartjs-2';


export default function PieChart({Income, Expense}) {

 console.log(Income, Expense)

    return (

        <React.Fragment>
            <Pie
            data={{
            datasets:[{
                data: [Income, Expense],
                backgroundColor: ["Orange", "Brown"]
            }],
            labels: ["Income", "Expense"]
            }}
            redraw={false}
        />
        </React.Fragment>
    )

}