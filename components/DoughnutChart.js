import React, {useState} from 'react';
import { Doughnut, Line } from 'react-chartjs-2';


export default function DoughnutChart({Income, Expense}) {

 console.log(Income, Expense)

    return (

        <React.Fragment>
            <Doughnut 
            data={{
            datasets:[{
                data: [Income, Expense],
                backgroundColor: ["blue", "red"]
            }],
            labels: ["Income", "Expense"]
            }}
            redraw={false}
        />
        </React.Fragment>
    )

}