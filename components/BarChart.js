import React, {useState} from 'react';
import { Doughnut, Line, Pie, Bar } from 'react-chartjs-2';


export default function BarChart({Income, Expense}) {

 console.log(Income, Expense)

    return (

        <React.Fragment>
            <Bar 
            data={{
            datasets:[{
                data: [Income, Expense],
                backgroundColor: ["Green", "Yellow"]
            }],
            labels: ["Income", "Expense"]
            }}
            redraw={false}
            />
        </React.Fragment>
    )

}