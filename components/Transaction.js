// // import Link from 'next/link'
// // import{ Jumbotron, Row, Col} from 'react-bootstrap'


// // export default function Banner({data}) {
// //     const { title, content, destination, label } = data;

// //     return (
        
// //         <Row>
// //             <Col>
// //                 <Jumbotron>
// //                     <h1>{title}</h1>
// //                     <p>{content}</p>
// //                     <Link href={destination}>
// //                         <a>
// //                             {label}
// //                         </a>
// //                     </Link>
// //                 </Jumbotron>
// //             </Col>
// //         </Row>

// //     )
// // }

// import React, { useState, useEffect } from 'react';
// import {Form, Button, Row, Col, Container} from 'react-bootstrap';
// // import Swal from 'sweetalert2'
// import UserContext from '../UserContext';
// // import Router from 'next/router';
// import Head from 'next/head';
// // import View from '../../components/View'
// import AppHelper from '../app-helper'

// export default function Home() {

// // const Transaction = () => {
        

//         return (
//             <React.Fragment>
//                 <Head>
//                     <title>Home Page</title>
//                 </Head>
//                 <Container className="mt-4">
//                     <Row>
//                         <Col xs={4}>
//                             <Form>
//                             <Form.Label><h2>Add New Transaction</h2></Form.Label>
//                             <div><h3>Balance: </h3></div>
//                             <br></br>
//                             <div><h4>Income: </h4></div>
//                             <div><h4>Expenses: </h4></div>
//                             <Form.Group> 
//                                 <Form.Label></Form.Label>
//                                 <Form.Control placeholder="Description" />
//                                     </Form.Group>
//                                     <Form.Group>
//                                         {/* <Form.Label></Form.Label> */}
//                                     <Form.Control placeholder="Amount" />
//                                     </Form.Group>
//                                     <Form.Group >
//                                         {/* <Form.Label></Form.Label> */}
//                                     <Form.Control as="select" placeholder="Transaction Type">
//                                         <option>Transaction Type</option>
//                                         <option>Income</option>
//                                         <option>Expense</option>
//                                     </Form.Control>
//                                     </Form.Group>
//                                     <Form.Group >
//                                         {/* <Form.Label></Form.Label> */}
//                                     <Form.Control as="select" placeholder="Choose Category">
//                                         <option>Choose Category</option>
//                                     </Form.Control>
//                                         <Button type="submit" className="mb-2" size="sm" block>
//                                             ADD CATEGORY
//                                         </Button>
//                                     </Form.Group>
//                                 <Button variant="info" type="submit" id="submitBtn" size="lg" block>
//                                     ADD TRANSACTION
//                                 </Button> 
//                             </Form>
//                         </Col >
//                         <Col xs={8}>
//                             <Form>
//                                 <Form.Group> 
//                                     <Form.Label><h1>Transactions</h1></Form.Label>
                                    

//                                 </Form.Group>
//                             </Form>
//                         </Col >
//                     </Row>
//                 </Container>
//             </React.Fragment>


//         )
//     }
// // }


// // modify the course components from the previous sessions activity
// import { useState, useEffect } from 'react';
// import {Row, Col, Card, Button, Container} from 'react-bootstrap';
// import PropTypes from 'prop-types';
// import 'bootstrap/dist/css/bootstrap.min.css';
// import Swal from 'sweetalert2';
// //Create a new component named Course.js where we can import and show a bootstrap-react Card component with the following information:

// //A course name named Sample Course
// //Course description with placeholder text
// //Any course price

// //Import a bootstrap-react Button component and also show it in the bottom of the card as an "Enroll" button

// //In Home.js, display our Course component underneath the Highlights component


// //Modify the course component from the previous session's activity so that it can have an actual course data passed in as a prop

// //Use a state hook on the course component to demonstrate component state by keeping track of the number of enrollees


// // put course as prop name
// export default function Course({course}) {
//     // destructure the course prop into its properties
//     const { name, description, price, start_date, end_date } = course

//     // use state hook for this component to be able to store its state
//     const [count, setCount] = useState(0)
//     const [slots, setSlots] = useState(10)
//     const [isOpen, setIsOpen] = useState(true);

//     useEffect(() => {
//         if( slots === 0 ) {
//             setIsOpen(false)
//         }
//     },[slots]) //second argument for useEffect ALWAYS has to be an array
//     // empty array: in useEffect watches ALL your states

//     function enroll(courseId) {

//         fetch('http://localhost:4000/api/users/enroll', {
//             method: 'POST',
//             headers: {
//                 'Content-Type': 'application/json',
//                 'Authorization': `Bearer ${localStorage.getItem('token')}`
//             },
//             body: JSON.stringify({
//                 courseId: courseId
//             })
//         })
//         .then(res => res.json())
//         .then(data => {
            
//             console.log(data)

//             if (data === true) {

//                 Swal.fire('Thanks you!', `You have successfully enrolled in ${course.name}.`, 'success')

//             } else {

//                 Swal.fire('Oopss!', `Something went wrong Please try again.`, 'error')
//             }
//         })
//     }
    

//     return (
//         <Container>
//         <Row>
//             <Col xs={12} md={12}>
//                 <Card className="cardHighlight">
//                     <Card.Body>
//                     <Card.Title>
//                         <h2>{name}</h2>
//                     </Card.Title>
//                     <Card.Subtitle>
//                         <h4>{description}</h4>
//                     </Card.Subtitle>
//                     <Card.Text>
//                     Pariatur adipisicing aute do amet dolore cupidatat. 
//                     Eu labore aliqua eiusmod commodo occaecat mollit ullamco 
//                     labore minim. Minim irure fugiat anim ea sint consequat  
//                     </Card.Text>
//                     <Card.Text>
//                     <h4>Price: {price}</h4>

//                     <h4>Enrolees:</h4>
//                     <p>{count}</p>

//                     <h4>Slots:</h4>
//                     <p>{slots}</p>

//                     <h4>Start Date:</h4>
//                     <p>{start_date}</p>

//                     <h4>End Date:</h4>
//                     <p>{end_date}</p>
//                     </Card.Text>
//                     {isOpen ? 
//                         <Card.Footer>
//                             <Button variant="info" onClick={() => {enroll(course._id)}}>Enroll</Button>{''}
//                         </Card.Footer>
//                         :
//                         <Card.Footer>
//                             <Button variant="info" disabled>Enroll</Button>{''}
//                         </Card.Footer>
//                     }

//                     </Card.Body>
//                 </Card>
//             </Col>
//         </Row>
//         </Container>
//     )
// }

// // Create another state hool on the Course components representing the number of available seats
// // for the course. Every course will have 30 seats available initially.
// // show this component as well.

// // We will create a button of enroll, enable si enroll kapag may available seats
// // if no available seats, disable si enroll button

// // Step 1: Create a function that will represent an enrollment: a seat will be taken and the number
// // of enrollees will go up. If seats are not available, disable enroll button,
// // if available set to enable enroll button

// // Step 2: Declare the state variable needed

// // Step 3: Modify the course enrollment button to invoke ths newly created function when clicked

// //you can add the availability of seats and number of enrollees in Card.text





// //import PropTypes from 'prop-types';

// //Add PropTypes in the Course component to validate props
// //to check that the Course component is getting the correct prop types
// Course.propTypes = {
//     // shape() is used to check that a prop object conforms to a specific shape
//     course: PropTypes.shape({
//         name: PropTypes.string.isRequired,
//         description: PropTypes.string.isRequired,
//         price: PropTypes.number.isRequired
//     })
// }