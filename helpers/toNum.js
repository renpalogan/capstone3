export default function toNum(str) {

    // converting string to array form
    const stringArray = [...str]
    const filteredArray = stringArray.filter( element => element !== "," )

    // x - initial value
    // y - current value
    return parseInt(filteredArray.reduce((x,y) => x + y))

}


/*
step 1. convert string to an array form
step 2. filter out the unnecessary element on our array
step 3. reduce the filtered array back to a single string, without spaces and commas
step 4. return the string to a parse integer
*/